package com.panneerat.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;
    public HelloMyName(){
        super("Hello My Name");

        lblName = new JLabel("Name: ");
        lblName.setBounds(20, 20, 50, 20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(75, 20, 220, 20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(90, 50, 195, 20);
        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello " + myName);
                
            }
            
        });

        lblHello = new JLabel("Hello My Name");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(90, 100, 195, 20);

        this.add(lblName);
        this.add(txtName);
        this.add(btnHello);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        this.setVisible(true);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();

    }

}
